
*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary
Library           Screenshot

*** Variables ***
${LOGIN URL}      https://172.31.31.168:2443/swp/customgroup/saa
${BROWSER}        Chrome
${username}       manu
${password}       Venus2009Venus2009+
${false_username}    manu
${false_password}    manu123


*** Test Cases ***
Akura Login_Logout
    Open Browser To Login Page
    Input Username
    Input Password
    Select Akura
    Take Screenshot
    Login
    Logout
    # Welcome Page Should Be Open
    [Teardown]    Close Browser

Torch Login_Logout
    Open Browser To Login Page
    Input Username
    Input Password
    Select Torch
    Take Screenshot
    Login
    Logout
    # Welcome Page Should Be Open
    [Teardown]    Close Browser


*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Sleep   2s 
    Click Button    //button[contains(@id,"details-button")]
    Sleep   2s
    Click Element   //a[contains(@id,"proceed-link")]
    Sleep   2s
    Click Element   //*[contains(@class,"auth_OL") ]
    Sleep   2s
Input Username
    # [Arguments]    ${username}
    Input Text    //*[contains(@id,"gwt-debug-platform_login-username") ]    ${username}
    Sleep   2s

Input Password
    # [Arguments]    ${password}
    Input Text     //*[contains(@id,"gwt-debug-platform_login-password") ]    ${password}
    Sleep   2s

Select Akura
    # [Arguments]    ${password}
    Sleep   2s
    Click Element   //*[contains(@class,'auth_NUC')]
    Sleep   2s
    Click Element   //*[contains(@class,'auth_DCC auth_FDC auth_GCC auth_ADC auth_CTC')]

Select Torch
    # [Arguments]    ${password}
    Sleep   2s
    Click Element   //*[contains(@class,'auth_NUC')]
    Sleep   2s
    Click Element   //div[6]/div/div/table/tbody[1]/tr[3]

Login
    Sleep   1s
    Click Button    gwt-debug-platform_login-logon
    # Click Button    //button[contains(@class,'auth_NCB auth_A5')]
    Title Should Be  Netlink-Cobra SAA Admin 7.6

Logout
    Wait Until Element is Visible   //*[contains(@id,'gwt-debug-desktop-topmenu-Logout')]
    Take Screenshot
    Click Element   //*[contains(@id,'gwt-debug-desktop-topmenu-Logout')]
    Sleep   2s
    Click Button    //button[contains(@id,'gwt-debug-dialog-ask-0-ok')]
    

# Welcome Page Should Be Open
#     Title Should Be    Welcome Page

# wait until auth_JMC is visible
# auth_BMC should contain Login has failed.