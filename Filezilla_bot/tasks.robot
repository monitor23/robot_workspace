*** Settings ***
Documentation     Spotify Windows desktop application robot. Opens the Spotify
...               desktop application, searches for the given song, and plays the
...               song. Demonstrates the basic Windows-automation capabilities of
...               the RPA Framework, using keyboard navigation.
Library           Sample_modder.py
Library           RPA.Desktop

*** Variables ***
${EXECUTABLE_NAME}=    filezilla
${sample_path}=        /home/netlink/Desktop/robot_workspace/Filezilla_bot/samples/SU3

*** Keywords ***
Change Messages
    
    Click    alias:back     double click
    Sleep  2s

*** Keywords ***
Open Filezilla
    Open application    ${EXECUTABLE_NAME}

*** Keywords ***
Open Sitemanager
    Press keys    ctrl    s

*** Keywords ***
Create Inside Network Connection

    #Alt + t  = Host
    #Alt + w  = Password
    #Alt + p  = Port
    #Alt + l  = User
    #Alt + c  = connect

    Sleep  2s
    Press Keys    alt     n
    Sleep  2s
    Type text     Inside the network
    Sleep  2s
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Sleep  1s
    Press Keys    tab
    Press Keys    enter
    
    Press Keys    down
    Press Keys    enter
    Sleep  2s
    Press Keys    alt     t
    Type text     172.31.31.198
    Sleep  2s
    Press Keys    alt     p
    Type text     21
    Sleep  2s
    Press Keys    alt     l
    Type text     swiftuser3
    Sleep  2s
    Press Keys    alt     w
    Type text     Password1!
    Sleep  2s
    Press Keys    alt     o
    Sleep  2s
    Press Keys    enter

*** Keywords ***
Create Outside Network Connection

    #Alt + t  = Host
    #Alt + w  = Password
    #Alt + p  = Port
    #Alt + l  = User
    #Alt + c  = connect
    Sleep  2s
    Press Keys    alt     n
    Sleep  2s
    Type text     Outside the network
    Sleep  2s
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Press Keys    tab
    Sleep  1s
    Press Keys    tab
    Press Keys    enter
    
    Press Keys    down
    Press Keys    enter
    Sleep  2s
    Press Keys    alt     t
    Type text     secureftp.netlink-testlabs.com
    Sleep  2s
    Press Keys    alt     p
    Type text     8221
    Sleep  2s
    Press Keys    alt     l
    Type text     swiftuser3
    Sleep  2s
    Press Keys    alt     w
    Type text     Password1!
    Sleep  2s
    Press Keys    alt     o
    Sleep  2s
    Press Keys    enter  

*** Keywords ***
Open Existing Connection
    Wait For Element    alias:collapse_button
    Click    alias:collapse_button
    Sleep  1s

*** Keywords ***
Connect To Inside Network Connection
    Wait For Element    alias:inside_network
    Click    alias:inside_network
    Sleep  5s

*** Keywords ***
Expand Swiftuser Folder
    Wait For Element    alias:sw3
    Click    alias:sw3  double click
    Sleep  3s

*** Keywords ***
Click MT
    Wait For Element    alias:MT
    Click    alias:MT
    Sleep  3s


*** Keywords ***
Click MX
    Wait For Element    alias:MX
    Click    alias:MX   
    Sleep  3s

*** Keywords ***
Click MTLAU
    Wait For Element    alias:MTLAU
    Click    alias:MTLAU   
    Sleep  3s

*** Keywords ***
Click MXLAU
    Wait For Element    alias:MXLAU
    Click    alias:MXLAU
    Sleep  3s

*** Keywords ***
Double Click MT
    Wait For Element    alias:MT
    Click    alias:MT   double click
    Sleep  3s

*** Keywords ***
Double Click MX
    Wait For Element    alias:MX
    Click    alias:MX   double click
    Sleep  3s

*** Keywords ***
Double Click MTLAU
    Wait For Element    alias:MTLAU
    Click    alias:MTLAU   double click
    Sleep  3s

*** Keywords ***
Double Click MXLAU
    Wait For Element    alias:MXLAU
    Click    alias:MXLAU   double click
    Sleep  3s


*** Keywords ***
Click Fromswift
    Wait For Element    alias:fromswift_uo
    Click    alias:fromswift_uo    double click
    Sleep  3s

*** Keywords ***
Click Toswift
    Wait For Element    alias:toswift
    Click    alias:toswift_uo  double click
    Sleep  3s

*** Keywords ***
Click Back
    Wait For Element    alias:back
    Click    alias:back     double click
    Sleep  2s


*** Keywords ***
Navigate to samples
    Wait For Element    alias:home
    Click    alias:home     double click
    Sleep  2s

    Wait For Element    alias:netlink
    Click    alias:netlink     double click
    Sleep  2s

    Wait For Element    alias:samples
    Click    alias:samples     double click
    Sleep  2s

*** Keywords ***
Change Sender References
    Sample_modder.MessageReferenceChanger
    log     Refrence for Sample Messages changed 

*** Tasks ***
Open Filezilla and Perform SW3 Testing
    Open Filezilla
    Sleep  2s
    
    #### Remove the following comments when setting up a new environment ###
    # Open Sitemanager
    # Sleep  5s
    # #Enter Connection Details
    # Create Outside Network Connection
    # Sleep  5s
    # Open Sitemanager
    # Sleep  2s
    # Create Inside Network Connection
    ########################################################################
    
    ######  Existing Connection  ########
    Change Sender References
    Open Existing Connection
    Connect To Inside Network Connection
    Expand Swiftuser Folder
    Click MTLAU
    # Click Toswift
    Click MXLAU
    # Click Toswift
    Click MT
    # Click Toswift
    # Click Fromswift
    Click MX
    # Click Toswift
    Sleep  10s
    [Teardown]    Close All Applications

