*** Settings ***
Documentation   Template robot main suite.
Library           SeleniumLibrary
Library           Screenshot


*** Variables ***

# Browser
${Broswer}          Chrome


# Website links
${redmine}              https://redmine.netlink-group.com/
${confluence}           https://confluence.netlink-testlabs.com/login.action?os_destination=%2F
${horizon}              https://horizon.netlink-testlabs.com/portal/webclient/index.html#/
${testlink}             http://testlink.netlink-testlabs.com/
${remote_app}           https://remoteapp.netlink-testlabs.com/
${gitlab}               https://gitlab.netlink-testlabs.com/
${netlink-testlabs}     https://netlink-testlabs.com
${netlink-group}        https://netlink-group.com
${netlinksc}            https://netlinksc.com
${netlinksdn}           https://netlinksdn.com
${mantis}               http://mantis.netlink-testlabs.com/
${jenkins}              http://jenkins.netlink-testlabs.com
${jenkins2}             http://jenkins2.netlink-testlabs.com
${filestash}            http://172.31.31.150:8334/login
#Credentials : Username

${redmine_username}          asad
${confluence_username}       asad
${horizon_username}          asad
${testlink_username}         asad
${remote_app_username}       ad\\asad
${gitlab_username}           asad
${mantis_username}           netlink
${jenkins_username}          manu
${jenkins2_username}         selenium
${filestash_username}        swiftuser1

#Credentials : Passwords

${redmine_password}          abcd1234
${confluence_password}       Qwerty@123
${horizon_password}          Qwerty@123
${testlink_password}         Password1!
${remote_app_password}       Qwerty@123
${gitlab_password}           Gem@123#
${mantis_password}           adsl55
${jenkins_password}          Password1!
${jenkins2_password}         Password1!
${filestash_password}        Password1!

# logout url

${logout_url}               http://testlink.netlink-testlabs.com/logout.php?viewer=


# Hosts

${filestash_host}           secureftp.netlink-testlabs.com


*** Test Cases ***

Check redmine
    Open Broswer To Page    ${redmine}
    Enter Username  ${redmine_username}
    Enter Password  ${redmine_password}
    Submit Credentials
    Logout

Check testlink
    Open Broswer To    ${testlink}
    Enter tl_Username  ${testlink_username}
    Enter tl_Password  ${testlink_password}
    Submit tl_Credentials
    tl_Logout

Check Horizon
    Open Broswer To    ${horizon}
    Enter Username  ${horizon_username}
    Enter Password  ${horizon_password}
    Submit H_Credentials
    Sleep  5s
    h_Logout

Check RemoteApp
    Open Broswer To    ${remote_app} 
    Enter ra_Username  ${remote_app_username} 
    Enter ra_Password  ${remote_app_password}
    Submit ra_Credentials
    ra_Logout

Check Confluence
    Open Broswer To    ${confluence} 
    Enter c_Username  ${confluence_username} 
    Enter c_Password  ${confluence_password}
    Submit c_Credentials
    c_Logout

Check Gitlab
    Open Broswer To    ${gitlab} 
    Enter g_Username  ${gitlab_username} 
    Enter g_Password  ${gitlab_password}
    Submit g_Credentials
    g_Logout

Check testlabs
    Open Broswer To sdc  ${netlink-testlabs}
    Wait Until Element Is Visible       //*[contains(@class,'wpb_revslider_element wpb_content_element')]
    Sleep  1s
    Take Screenshot

Check netlink
    Open Broswer To    ${netlink-group}
    Wait Until Element Is Visible       //*[contains(@class,'elementor-widget-wrap')]
    Sleep  1s
    Take Screenshot

Check netlinksc
    Open Broswer To    ${netlinksc} 
    Wait Until Element Is Visible       //*[contains(@id,'rev_slider_1_1_wrapper')]
    Sleep  1s
    Take Screenshot

Check netlinksdn
    Open Broswer To    ${netlinksdn} 
    Wait Until Element Is Visible       //*[contains(@class,'elementor-background-overlay')]
    Sleep  1s
    Take Screenshot

Check Mantis
    Open Broswer mantis    ${mantis} 
    Enter m_Username  ${mantis_username} 
    Enter m_Password  ${mantis_password}
    m_Logout

Check jenkins
    Open Broswer To    ${jenkins} 
    Enter j_Username  ${jenkins_username} 
    Enter j_Password  ${jenkins_password}
    Submit j_credentails
    j_Logout

Check jenkins_2
    Open Broswer To    ${jenkins2} 
    Enter j_Username  ${jenkins2_username} 
    Enter j_Password  ${jenkins2_password}
    Submit j_credentails
    j_Logout

Check filestash
    Open Broswer To    ${filestash} 
    Enter Hostname     ${filestash_host}
    Enter Username     ${filestash_username} 
    Enter Password     ${filestash_password}
    Submit fs_credentails
    fs_Logout

Termination
    End All

*** Keywords ***

Open Broswer To Page
    [Arguments]    ${server}
    Open Browser    ${server}    ${Broswer} 
    Click Button    //button[contains(@id,"details-button")]
    Sleep   2s
    Click Element   //a[contains(@id,"proceed-link")]
    Sleep   2s
    # Click Element   //*[contains(@class,"auth_OL") ]
    # Sleep   2s

Open Broswer To sdc
    [Arguments]    ${server}
    Open Browser    ${server}    ${Broswer} 
    Click Button    //button[contains(@id,"details-button")]
    Sleep   2s
    Click Element   //a[contains(@id,"proceed-link")]
    Sleep   2s
    Click Button    //button[contains(@id,"details-button")]
    Sleep   2s
    Click Element   //a[contains(@id,"proceed-link")]
    Sleep   2s
    # Click Element   //*[contains(@class,"auth_OL") ]
    # Sleep   2s

Enter Username
    [Arguments]    ${username}
    Wait Until Element Is Visible   username
    Input Text    username    ${username}

Enter Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button    login

Logout
    Take Screenshot
    Click Element   //*[contains(text(),'Sign out')]


Open Broswer To
    [Arguments]    ${server}
    Open Browser    ${server}    ${Broswer} 

Enter tl_Username
    [Arguments]    ${username}
    Input Text    tl_login       ${username}

Enter tl_Password
    [Arguments]    ${password}
    Input Text    tl_password    ${password}

Submit tl_Credentials
    Click Element    //*[contains(@id,'tl_login_button')]
    Take Screenshot

tl_Logout
    Go To    ${logout_url}
    # Wait Until Element Is Visible    //img[contains(@src, 'http://testlink.netlink-testlabs.com/gui/themes/default/images/computer_go.png')]
    # Click Element   //img[contains(@src, 'http://testlink.netlink-testlabs.com/gui/themes/default/images/computer_go.png')]
    # //a[contains(@href,'logout.php')]

Submit H_Credentials
    Click Button    //Button[contains(@id,'loginButton')]
    

h_Logout
    Wait Until Element Is Visible   //*[contains(@id,'logoutBtn')]
    Click Element   //*[contains(@id,'logoutBtn')]
    Take Screenshot
    Click Button    //*[contains(@id,'okDialogBtn')]

Enter ra_Username
    [Arguments]    ${username}
    Input Text    DomainUserName       ${username}

Enter ra_Password
    [Arguments]    ${password}
    Input Text    UserPass    ${password}

Submit ra_Credentials
    Click Element    //*[contains(@id,'btnSignIn')]
    Take Screenshot

ra_Logout
    Wait Until Element Is Visible   //*[contains(@id,'PORTAL_SIGNOUT')]
    Click Element   //*[contains(@id,'PORTAL_SIGNOUT')]

Enter c_Username
    [Arguments]    ${username}
    Input Text    os_username       ${username}

Enter c_Password
    [Arguments]    ${password}
    Input Text    os_password    ${password}

Submit c_Credentials
    Click Element    login
    Take Screenshot

c_Logout
    Wait Until Element Is Visible   //*[contains(@id,'user-menu-link')]
    Click Element   //*[contains(@id,'user-menu-link')]
    Wait Until Element Is Visible   //*[contains(@id,'logout-link')]
    Click Element   //*[contains(@id,'logout-link')]

Enter g_Username
    [Arguments]    ${username}
    Input Text    user[login]       ${username}

Enter g_Password
    [Arguments]    ${password}
    Input Text    user[password]    ${password}

Submit g_Credentials
    Click Button    commit

g_Logout
    Wait Until Element Is Visible   //*[contains(@class,'header-user-dropdown-toggle')]
    Take Screenshot
    Click Element   //*[contains(@class,'header-user-dropdown-toggle')]
    Wait Until Element Is Visible   //*[contains(@class,'sign-out-link')]
    Click Element   //*[contains(@class,'sign-out-link')]

Open Broswer mantis
    [Arguments]    ${server}
    Open Browser    ${server}    ${Broswer} 
    Click Element    //*[contains(@id,"bitnami-link")]

Enter m_Username
    [Arguments]    ${username}
    Input Text    username       ${username}
    Click Element   //*[contains(@class,'btn-success')]

Enter m_Password
    [Arguments]    ${password}
    Input Text    password    ${password}
    Click Element   //*[contains(@class,'btn-success')]
    

m_Logout
    Take Screenshot
    Click Element     //*[contains(@class,'user-info')]
    Click Element     //a[contains(text(),' Logout')]

Enter j_Username
    [Arguments]    ${username}
    Input Text    j_username       ${username}

Enter j_Password
    [Arguments]    ${password}
    Input Text    j_password    ${password}
    
Submit j_credentails
    Click Button    Submit

j_Logout
    Take Screenshot
    Click Element     //div[2]/header/div[3]/a[2]

Enter Hostname
    [Arguments]    ${hostname}
    Input Text      hostname    ${filestash_host}

Submit fs_credentails
    Click Button    CONNECT

fs_Logout
    Take Screenshot
    Wait Until Element Is Visible   //img[contains(@class,'component_icon')]
    Click Element   //img[contains(@class,'component_icon')]

End All
    Close All Browsers